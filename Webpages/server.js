const express = require('express');
const app = express();

// Define o diretório onde estão os arquivos estáticos (CSS, imagens, etc)
app.use(express.static(__dirname + '/public'));

// Define as rotas para cada página
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/home.html');
});

app.get('/objetivo', (req, res) => {
  res.sendFile(__dirname + '/views/objetivo.html');
});

app.get('/desenvolvimento', (req, res) => {
  res.sendFile(__dirname + '/views/desenvolvimento.html');
});

app.get('/conclusao', (req, res) => {
  res.sendFile(__dirname + '/views/conclusao.html');
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port http://localhost:${PORT}`);
});

